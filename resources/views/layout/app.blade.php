@extends('layout.master')

@section('master.content')
    @include('partials.main-nav')

    <div class="my-content">
        @include('partials.header')
        @include('partials.subheader')

        <section id="main" class="Main">
            <div class="row">
                @yield('content')
            </div>
        </section>
    </div>

    @include('partials.footer')

@endsection