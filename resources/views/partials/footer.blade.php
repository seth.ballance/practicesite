<footer class="footer">
    <div class="container">
        <div class="content has-text-centered">
            <p>
                <strong>MySite</strong> by <a href="{{ route('welcome') }}">Seth Ballance</a>
            </p>
            <p>
                <a class="icon" href="https://www.facebook.com/saballance">
                    <i class="fa fa-facebook-square"></i>
                </a>
                <a class="icon" href="https://www.instagram.com/seth12thman/?hl=en">
                    <i class="fa fa-instagram"></i>
                </a>
            </p>
        </div>
    </div>
</footer>