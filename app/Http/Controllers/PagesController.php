<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{

    /**
     * Display welcome page.
     */
    public function welcome()
    {
        return view('welcome');
    }

    /**
     * Display welcome page.
     */
    public function about()
    {
        return view('about');
    }

    /**
     * Display welcome page.
     */
    public function stars()
    {
        return view('stars');
    }
}